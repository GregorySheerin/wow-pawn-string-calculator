﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WowRaidStatCalcTool
{
    //DESCRIP:This class is meant to define a raider
    //a raider is a person,with stats in the raid
    //Each raider will have a set of stats to compare to each other

    class RaidMemeber
    {
        public int RaiderID { get; set; } //id for unquie Idenitafcation
        public string RaiderName { get; set; } //Name of the Player EG:Greg
        public string RaiderClass { get; set; } //Class the Player is Playing EG:Warrior
        public string RaiderClassSpec { get; set; } //Specilastion of players class EG: Fury
        public string RaiderPawnString { get; set; } //string that players get from SIMC after calcing stat values(Possible Admin Issue,players can )
        public double PrimaryWeight { get; set; } //Strength/Intellect/Agiity NOTE: calcuations are the same regardless of which one of 3 the player is using
        public double CritWeight { get; set; } //Weight of Crital Strike Rating
        public double MastWeight { get; set; } //Weight of Mastery Rating
        public double VersWeight { get; set; } //Weight of Versatilty
        public double HasteWeight { get; set; } //Weight of Haste
        public double LeechWeight { get; set; } //Weight of Leech NOTE:Only applicple to tanks and healers,weight will be 0 for others,thus not affect calcuations
        public double ArmorWeight { get; set; } //Weight of Armor stat NOTE:Only applicple to tanks,weight will be 0 for others,thus not affect calcuations
        public double StamWeight { get; set; } //Weight of Avoidance stat NOTE:Only applicple to tanks,weight will be 0 for others,thus not affect calcuations
        public double AvoidanceWeight { get; set; } //Weight of Avoidance stat NOTE:Only applicple to tanks,weight will be 0 for others,thus not affect calcuations


        public RaidMemeber ReadPawnString(string pawnstring)
        {
            // Sample Pawn String :"( Pawn: v1: \"Ketsuki\": Strength =12.55, CritRating=10.35, HasteRating=10.96, MasteryRating=10.26, Versatility=10.92 )";

            RaidMemeber returnRaidMemeber = new RaidMemeber(); //return the data that we pull from the pawn string
            char[] seperatorCharacters = {':', ',' }; //these are characters that seperate out different elements of the string
            pawnstring = pawnstring.Replace("(", ""); //get rid of the brackets that the string comes with by default
            pawnstring = pawnstring.Replace(")", "");
            pawnstring = pawnstring.Replace("Pawn: v1:",""); //get rid of the pawn version number,again no need at all for this
            pawnstring = pawnstring.Replace("\"",""); //might be able to get rid of this later
            string[] pawnArray = pawnstring.Split(seperatorCharacters);

            //now that the data is tidyed up and split up,we can begin to build our object

            returnRaidMemeber.RaiderName = pawnArray[0].Split('=').Last();
            returnRaidMemeber.RaiderClass = pawnArray[1].Split('=').Last();
            returnRaidMemeber.RaiderClassSpec = pawnArray[2].Split('=').Last();
            returnRaidMemeber.PrimaryWeight =Convert.ToDouble(pawnArray[3].Split('=').Last());
            //now here were things get complicated,all strings will follow the above format,but beyond this point they can be in differnt orderings
            //in the interest of simplicty on the users end,we have to abadon simplicty on our end
            //in order to ensure that the right data is assigned to the right varible in the object,we must search though the array to find the corisponding value

            foreach (var item in pawnArray)
            {
                //all characters have the below 
                if (item.Contains("CritRating"))
                {
                    returnRaidMemeber.CritWeight =Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("HasteRating"))
                {
                    returnRaidMemeber.HasteWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("MasteryRating"))
                {
                    returnRaidMemeber.MastWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("Versatility"))
                {
                    returnRaidMemeber.VersWeight = Convert.ToDouble(item.Split('=').Last());
                }

                //these 4 will not be included for all specs,only certain specs will worry about the below stats
                if (item.Contains("Leech"))
                {
                    returnRaidMemeber.LeechWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("Armor"))
                {
                    returnRaidMemeber.ArmorWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("Avoidance")) //this might need to be changed since I cant find a character to get a pawn string with avoidance in it
                {
                    returnRaidMemeber.AvoidanceWeight = Convert.ToDouble(item.Split('=').Last());
                }

                if (item.Contains("Stamina")) //this might need to be changed since I cant find a character to get a pawn string with avoidance in it
                {
                    returnRaidMemeber.StamWeight = Convert.ToDouble(item.Split('=').Last());
                }

            }

          
            return returnRaidMemeber;

        }
    }
}
