﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WowRaidStatCalcTool
{
    //this class is here to define what an item is
    //its stats mainly,and the calculations for the stats
    class Items
    {
        public double PrimaryStat { get; set; } //Strength/Intellect/Agiity NOTE: calcuations are the same regardless of which one of 3 the player is using
        public double CritStat { get; set; } //Weight of Crital Strike Rating
        public double MastStat { get; set; } //Weight of Mastery Rating
        public double VersStat { get; set; } //Weight of Versatilty
        public double HasteStat { get; set; } //Weight of Haste
        public double LeechStat { get; set; } //Weight of Leech NOTE:Only applicple to tanks and healers,weight will be 0 for others,thus not affect calcuations
        public double ArmorStat { get; set; } //Weight of Armor stat NOTE:Only applicple to tanks,weight will be 0 for others,thus not affect calcuations
        public double StamStat { get; set; } //Weight of Avoidance stat NOTE:Only applicple to tanks,weight will be 0 for others,thus not affect calcuations
        public double AvoidanceStat { get; set; } //Weight of Avoidance stat NOTE:Only applicple to tanks,weight will be 0 for others,thus not affect calcuations


        public double CalcItemValue(RaidMemeber memeber,Items NewItem) //this method is given the weights and then,runs the calculation for the raider
        {
            //Formula work by multping the amount of a stat by its statWeight
            //item value is then determined by adding all values together
            double returnValue = (memeber.PrimaryWeight * NewItem.PrimaryStat) + (memeber.CritWeight * NewItem.CritStat)  +(memeber.HasteWeight * NewItem.HasteStat) +(memeber.MastWeight * NewItem.MastStat) + (memeber.VersWeight * NewItem.VersStat) + (memeber.StamWeight * NewItem.StamStat) + (memeber.LeechWeight * NewItem.LeechStat) + (memeber.AvoidanceWeight * NewItem.AvoidanceStat) + (memeber.ArmorWeight * NewItem.ArmorStat);
            return returnValue;
        }
    }


}
